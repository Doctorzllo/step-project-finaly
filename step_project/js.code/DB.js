const BLOCKS = {
    graphicDesign : 'graphic-Design',
    webDesign : 'web-Design',
    landPages : 'land-Pages',
    Wordpress : 'word-Press'

}
export const DB =
    [{
        "url": "./img/amazing-work-1.png",
        "block": BLOCKS.graphicDesign
    }, {
        "url": "./img/amazing-work-2.png",
        "block": BLOCKS.webDesign
    }, {
        "url": "./img/amazing-work-3.png",
        "block": BLOCKS.webDesign
    }, {
        "url": "./img/amazing-work-4.png",
        "block": BLOCKS.landPages
    }, {
        "url": "./img/amazing-work-5.png",
        "block": BLOCKS.Wordpress
    }, {
        "url": "./img/amazing-work-6.png",
        "block": BLOCKS.landPages
    }, {
        "url": "./img/amazing-work-7.png",
        "block": BLOCKS.Wordpress
    }, {
        "url": "./img/amazing-work-8.png",
        "block": BLOCKS.Wordpress
    }, {
        "url": "./img/amazing-work-9.png",
        "block": BLOCKS.Wordpress
    }, {
        "url": "./img/amazing-work-10.png",
        "block": BLOCKS.Wordpress
    }, {
        "url": "./img/amazing-work-11.png",
        "block": BLOCKS.graphicDesign
    }, {
        "url": "./img/amazing-work-1.png",
        "block": BLOCKS.graphicDesign
    }, {
        "url": "./img/amazing-work-2.png",
        "block": BLOCKS.webDesign
    }, {
        "url": "./img/amazing-work-3.png",
        "block": BLOCKS.webDesign
    }, {
        "url": "./img/amazing-work-4.png",
        "block": BLOCKS.landPages
    }, {
        "url": "./img/amazing-work-5.png",
        "block": BLOCKS.Wordpress
    }, {
        "url": "./img/amazing-work-6.png",
        "block": BLOCKS.landPages
    }, {
        "url": "./img/amazing-work-7.png",
        "block": BLOCKS.Wordpress
    }, {
        "url": "./img/amazing-work-8.png",
        "block": BLOCKS.Wordpress
    }, {
        "url": "./img/amazing-work-9.png",
        "block": BLOCKS.Wordpress
    }, {
        "url": "./img/amazing-work-10.png",
        "block": BLOCKS.Wordpress
    }, {
        "url": "./img/amazing-work-11.png",
        "block": BLOCKS.graphicDesign
    }, {
        "url": "./img/amazing-work-1.png",
        "block": BLOCKS.graphicDesign
    }, {
        "url": "./img/amazing-work-2.png",
        "block": BLOCKS.webDesign
    }, {
        "url": "./img/amazing-work-3.png",
        "block": BLOCKS.webDesign
    }, {
        "url": "./img/amazing-work-4.png",
        "block": BLOCKS.landPages
    }, {
        "url": "./img/amazing-work-5.png",
        "block": BLOCKS.Wordpress
    }, {
        "url": "./img/amazing-work-6.png",
        "block": BLOCKS.landPages
    }, {
        "url": "./img/amazing-work-7.png",
        "block": BLOCKS.Wordpress
    }, {
        "url": "./img/amazing-work-8.png",
        "block": BLOCKS.Wordpress
    }]
