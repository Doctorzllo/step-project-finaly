const test = new Swiper(".thumbs-slider", {
    spaceBetween: 10,
    slidesPerView: 'auto',
    freeMode: true,
    watchSlidesProgress: true,
});

const swiperMain = new Swiper(".slider", {
    spaceBetween: 10,
    navigation: {
        nextEl: ".swiper-button-next ",
        prevEl: ".swiper-button-prev",
    },
    thumbs: {
        swiper: test,
    },
});