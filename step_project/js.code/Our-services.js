const image = document.getElementsByClassName("main-info-img")[0]
const text = document.getElementsByClassName("main-info-p")[0]
const buttons = document.getElementsByClassName('navigation-button' )

const switchBlockContent= (img , paragraph) =>{
    image.setAttribute('src', img)
    text.innerText = paragraph
}

export const clearUnActiveButtonsStyle = (buttons, mainStyle, activeStyle, additionalElementStyle) => {
    for (let i = 0; i < buttons.length; i++) {
        buttons[i].classList.remove(activeStyle)
        additionalElementStyle && buttons[i].classList.remove(additionalElementStyle)
        buttons[i].classList.add(mainStyle)
    }
}


export const makeButtonActive = (main, active, after) => (e , defaultButton) => {
    const target = e.currentTarget || defaultButton
    target.classList.remove(main)
    target.classList.add(active)
    after && target.classList.add(after)
}

const makeOurServicesButtonActive = makeButtonActive('main-services-btn', 'main-services-btn-active', 'element')

export const swapCategory = (img , paragraph) => (e)  => {
    switchBlockContent(img , paragraph)
    clearUnActiveButtonsStyle(buttons, 'main-services-btn', 'main-services-btn-active', 'element')
    makeOurServicesButtonActive(e, false)
}

makeOurServicesButtonActive(false , buttons[0])

    const ourServicesData = [
        {
            url: './img/services-img.png',
            title: 'НАПРАВЛЕНИЕ ДИЗАЙНА В НАШЕЙ АКАДЕМИИ ПРЕДСТАВЛЕНО КУРСАМИ GRAPHIC DESIGN BASIC, GRAPHIC DESIGN ADVANCED, WEB DESIGN, UX DESIGN, ILLUSTRATION BASIC И ILLUSTRATION ADVANCED.'
        },
        {
            url: './img/graphic-design.png',
            title: 'Lorem ipsum dolor sit amet,\n'+
                '\t\t\tconsectetur adip isicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Excepteur\n'+
                '\t\t\tsint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum'
        },
        {
            url: './img/online-support.jpg' ,
            title : 'Online support is a web-based form of delivering a customer service. Nowadays, it is a very popular form of contacting company, more popular than visiting a company or a phone call. Online support is also a part of customer relationship management.'
        },
        {
            url: './img/app-design.png' ,
            title : 'A curated collection of mobile app to inspire you in your mobile UI/UX design process.\n'+
                'This constantly-updated list featuring what find on the always-fresh Muzli inventory.\n'+
                'Even more examples of mobile apps could be found on our amazing visual search.'
        },
        {
            url: './img/online-marketing.jpg' ,
            title : 'Post Your Job & Get Competitive Prices From Internet Marketers. Get Started Now. 95% Rehire Rate. Pay When 100% Happy. 20M+ Jobs Completed.24/7 Support. Pricing For Any Budget. Award Winning Marketplace. 5-Star Satisfaction. Get Quotes in Minutes.'
        } ,
        {
            url: './img/SEO-Services.png',
            title : 'Thrive is an SEO company that understands your niche and can boost your ranking for specific search terms. Your SEO specialist will review the keywords your website is currently ranking for, take your keyword wishlist and conduct additional keyword research to build a list that makes the most sense for your website and the competitive landscape.'
        }

    ]


for (let i = 0; i < buttons.length; i++) {
    const {url, title} = ourServicesData[i]
    buttons[i].addEventListener('click', swapCategory(url, title))
}