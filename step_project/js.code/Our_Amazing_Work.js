import {clearUnActiveButtonsStyle, makeButtonActive} from "./Our-services.js";
import {DB} from './DB.js';

const BlockChamber = document.getElementsByClassName("our-design-content")[0]
const buttonsSwap = document.getElementsByClassName('btn-swap')
const loadMoreBtn = document.getElementsByClassName('load-btn')[0]

let cardsCounter = 12
let openedBlockName

const insertToParentElement = (blockName) => {
    openedBlockName = blockName
    DB.forEach( (item, index ) => {
        if (index >= cardsCounter) return
        if (blockName && item.block !== blockName) return

        const wrapper = document.createElement('div')
        wrapper.className = 'creative-design-check4'
        wrapper.setAttribute('data-block', item.block)

        const seccondaryWrapper = document.createElement('div')
        seccondaryWrapper.className = 'creative-design-box4'

        const buttonsWrapper = document.createElement('div')
        buttonsWrapper.className = 'btn-design'

        const button1 = document.createElement('button')
        button1.className = 'creative-design-btn1'

        const button2 = document.createElement('button')
        button2.className = 'creative-design-btn2'

        buttonsWrapper.appendChild(button1)
        buttonsWrapper.appendChild(button2)

        seccondaryWrapper.appendChild(buttonsWrapper)

        const h = document.createElement('h2')
        h.className = 'creative-design-h'
        h.innerText = 'CREATIVE DESIGN'

        const span = document.createElement('span')
        span.className = 'creative-design-span'
        span.innerText = 'Web Design'

        seccondaryWrapper.appendChild(h)
        seccondaryWrapper.appendChild(span)

        wrapper.appendChild(seccondaryWrapper)
        wrapper.appendChild(seccondaryWrapper)

        const img = document.createElement('img')
        img.className = 'design-img design-img4'
        img.setAttribute('src', item.url)

        wrapper.appendChild(img)

        BlockChamber.appendChild(wrapper)
    })

}

insertToParentElement()

const makeOurAmazingWorkButtonActive = makeButtonActive("btn-swap-menu", "btn-swap-menu-hover")

const swapIcon = (e) => {
    clearUnActiveButtonsStyle(buttonsSwap,"btn-swap-menu","btn-swap-menu-hover")
    makeOurAmazingWorkButtonActive(e, false )

    BlockChamber.innerHTML = '';
    insertToParentElement(e.target.getAttribute('data-blockName'))
}
makeOurAmazingWorkButtonActive(false , buttonsSwap[0])
for (let i = 0; i < buttonsSwap.length; i++) {
    buttonsSwap[i].addEventListener('click', swapIcon)
}

loadMoreBtn.addEventListener('click', () => {
    cardsCounter += 12

    BlockChamber.innerHTML = '';
    insertToParentElement()

    document.getElementsByClassName('our-design')[0].removeChild(document.getElementsByClassName("load-btn-container")[0])
})
